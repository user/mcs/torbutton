<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Om Tor">

<!ENTITY aboutTor.viewChangelog.label "Se Endringslogg">

<!ENTITY aboutTor.ready.label "Utforsk. Privat.">
<!ENTITY aboutTor.ready2.label "Du er klar for verdens mest private surfing opplevelse.">
<!ENTITY aboutTor.failure.label "Noe gikk galt!">
<!ENTITY aboutTor.failure2.label "Tor fungerer ikke i denne nettleseren.">

<!ENTITY aboutTor.search.label "Søk med DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Spørsmål?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Sjekk vår Tor Nettleser Håndbok »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Hånbok for Tor-nettleseren">

<!ENTITY aboutTor.tor_mission.label "The Tor Project er en US 501(c)(3) ideell organisasjon som fremmer menneskerettigheter og friheter ved å skape og distribuere anonymisering og personvernteknologi for fri og åpen kildekode, som støtter deres ubegrensede tilgjengelighet og bruk, og fremmer sin vitenskapelige og populære forståelse.">
<!ENTITY aboutTor.getInvolved.label "Bli involvert »">

<!ENTITY aboutTor.newsletter.tagline "Få de siste nyhetene fra Tor rett til innboksen din.">
<!ENTITY aboutTor.newsletter.link_text "Registrer deg for Tor-nyheter.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor er gratis å bruke, grunnet bidrag fra folk som deg.">
<!ENTITY aboutTor.donationBanner.buttonA "Donér nå">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "DONÉR NÅ">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Your donation will be matched by Friends of Tor, up to $100,000.">
