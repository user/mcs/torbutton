<!ENTITY torsettings.dialog.title "تنظیمات شبکه تور">
<!ENTITY torsettings.wizard.title.default "اتصال به تور">
<!ENTITY torsettings.wizard.title.configure "تنظیمات شبکه تور">
<!ENTITY torsettings.wizard.title.connecting "ارتباط پایدار است">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "زبان مرورگر تور">
<!ENTITY torlauncher.localePicker.prompt "لطفا یک زبان انتخاب کنید.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "برای اتصال به تور بر روی &quot;اتصال&quot; کلیک کنید.">
<!ENTITY torSettings.configurePrompt "اگر شما در کشورهایی هستید که سعی می کنند Tor را سانسور کنند (مانند مصر، چین، ترکیه)، یا اگر از یک شبکه خصوصی که نیاز به یک پروکسی دارد استفاده کنید؛ برای اعمال تنظیمات شبکه روی &quot;پیکربندی&quot; کلیک کنید.">
<!ENTITY torSettings.configure "پیکربندی">
<!ENTITY torSettings.connect "اتصال">

<!-- Other: -->

<!ENTITY torsettings.startingTor "در انتظار برای آغاز به کار تور...">
<!ENTITY torsettings.restartTor "راه اندازی دوباره تور">
<!ENTITY torsettings.reconfigTor "پیکربندی مجدد">

<!ENTITY torsettings.discardSettings.prompt "شما تنظیمات bridge تٌٌر را انجام داده اید و یا از پروکسی محلی استفاده می کنید.&#160; برای برقراری ارتباط مستقیم با شبکه تٌر، این تنظیمات باید حذف گردند.">
<!ENTITY torsettings.discardSettings.proceed "حذف تنظیمات و اتصال">

<!ENTITY torsettings.optional "اختیاری">

<!ENTITY torsettings.useProxy.checkbox "من برای دسترسی به اینترنت از پروکسی استفاده می‌کنم.">
<!ENTITY torsettings.useProxy.type "نوع پروکسی">
<!ENTITY torsettings.useProxy.type.placeholder "یک نوع پروکسی را انتخاب کنید">
<!ENTITY torsettings.useProxy.address "آدرس">
<!ENTITY torsettings.useProxy.address.placeholder "آدرس آی‌پی یا نام میزبان">
<!ENTITY torsettings.useProxy.port "پورت">
<!ENTITY torsettings.useProxy.username "نام کاربری">
<!ENTITY torsettings.useProxy.password "گذرواژه">
<!ENTITY torsettings.useProxy.type.socks4 "ساکس ۴">
<!ENTITY torsettings.useProxy.type.socks5 "ساکس ۵">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "به نظر می رسد شبکه ارتباطی شما به اینترنت، از یک فایروال استفاده می کند. یعنی فقط اجازه اتصال از یک سری درگاه های به خصوص را می دهد.">
<!ENTITY torsettings.firewall.allowedPorts "درگاه های مجاز">
<!ENTITY torsettings.useBridges.checkbox "Tor در کشور من فیلتر یا سانسور شده است">
<!ENTITY torsettings.useBridges.default "انتخاب یک پل داخلی">
<!ENTITY torsettings.useBridges.default.placeholder "یک پل انتخاب کنید">
<!ENTITY torsettings.useBridges.bridgeDB "درخواست یک پل از torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "حروف موجود در تصویر را وارد کنید">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "یک چالش جدید داشته باشید">
<!ENTITY torsettings.useBridges.captchaSubmit "درخواست">
<!ENTITY torsettings.useBridges.custom "استفاده از یک پل که آدرس آن را می دانم">
<!ENTITY torsettings.useBridges.label "اطلاعات پل را از یک منبع مورد اعتماد وارد کنید.">
<!ENTITY torsettings.useBridges.placeholder "نوع آدرس: درگاه - پورت (هر کدام در یک خط)">

<!ENTITY torsettings.copyLog "کپی گزارش وقایع Tor">

<!ENTITY torsettings.proxyHelpTitle "کمک پروکسی">
<!ENTITY torsettings.proxyHelp1 "اگر از طریق شبکه‌ی یک شرکت، مدرسه، یا دانشگاه به اینترنت متصل می‌شوید، ممکن است احتیاج به یک پروکسی داخلی داشته باشید. اگر از احتیاج به پروکسی داخلی مطمئن نیستید، به تنظیمات شبکه یک مرورگر دیگر یا تنظیمات شبکه سیستم خود نگاه کنید.">

<!ENTITY torsettings.bridgeHelpTitle "کمک برای پل ارتباطی">
<!ENTITY torsettings.bridgeHelp1 "پل‌ها رله‌هایی فهرست نشده هستند که مسدودیت ارتباط به شبکه Tor را سخت‌تر می‌کنند.&#160; هر نوعی از پل از روش مختلفی برای مقابله با سانسور استفاده می‌کند.&#160; نوع obfs ترافیک شما را شبیه نویزهای راندوم نشان می‌دهند و نوع meek ترافیک شما را به جای اتصال به Tor، در حال اتصال به آن خدمات نشان می‌دهد.">
<!ENTITY torsettings.bridgeHelp2 "به دلیل اینکه بعضی کشورها سعی بر مسدودسازی Tor دارند، بعضی از پل‌ها فقط در این کشورها کار می‌کنند.&#160; اگر مطمئن نیستید که کدام پل‌ها در کشور شما کار می‌کنند، اینجا را ببینید torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "لطفا صبر کنید. در حال برقراری ارتباط با شبکه تٌر.&amp;160; این پروسه ممکن است چند دقیقه به طول بینجامد.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "اتصال">
<!ENTITY torPreferences.torSettings "تنظیمات Tor">
<!ENTITY torPreferences.torSettingsDescription "مرورگر Tor ترافیک شما را از طریق شبکه Tor هدایت می‌کند، که به واسطه هزاران داوطلب در سراسر جهان به اجرا در‌آمده‌ است." >
<!ENTITY torPreferences.learnMore "اطلاعات بیشتر">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "اینترنت:">
<!ENTITY torPreferences.statusInternetTest "آزمایش">
<!ENTITY torPreferences.statusInternetOnline "روی خط ">
<!ENTITY torPreferences.statusInternetOffline "آفلاین">
<!ENTITY torPreferences.statusTorLabel "شبکه تور:">
<!ENTITY torPreferences.statusTorConnected "متصل">
<!ENTITY torPreferences.statusTorNotConnected "عدم اتصال">
<!ENTITY torPreferences.statusTorBlocked "بالقوه مسدود است">
<!ENTITY torPreferences.learnMore "اطلاعات بیشتر">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "شروع سریع">
<!ENTITY torPreferences.quickstartDescriptionLong "شروع سریع، بر اساس آخرین تنظیمات اتصال استفاده‌شده توسط شما، در هنگام راه‌اندازی مرورگر تور را به طور خودکار به شبکه تور متصل می‌کند.">
<!ENTITY torPreferences.quickstartCheckbox "اتصال به صورت خودکار در همه وقت">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "پل ها">
<!ENTITY torPreferences.bridgesDescription "پل‌ها به شما کمک می‌کنند که در مناطقی که Tor مسدود شده است، به شبکه Tor دسترسی پیدا کنید. وابسته به منطقه‌ای که در آن هستید، ممکن است یک پل بهتر از دیگری عمل کند.">
<!ENTITY torPreferences.bridgeLocation "موقعیت شما">
<!ENTITY torPreferences.bridgeLocationAutomatic "خودکار">
<!ENTITY torPreferences.bridgeLocationFrequent "مکان‌های متداول">
<!ENTITY torPreferences.bridgeLocationOther "سایر مکان‌ها">
<!ENTITY torPreferences.bridgeChooseForMe "یک پل برای من انتخاب کن…">
<!ENTITY torPreferences.bridgeBadgeCurrent "پل‌های فعلی شما">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "شما می‌توانید یک یا چند پل را ذخیره کنید و Tor انتخاب خواهد کرد که هنگام اتصال از کدام یک استفاده کند. Tor در صورت نیاز، به طور خودکار از پل دیگر استفاده خواهد کرد.">
<!ENTITY torPreferences.bridgeId "پل #1: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "پاک کردن">
<!ENTITY torPreferences.bridgeDisableBuiltIn "غیرفعال‌سازی پل‌های داخلی">
<!ENTITY torPreferences.bridgeShare "این پل را با استفاده از کد QR یا با کپی آدرس آن به اشتراک بگذارید:">
<!ENTITY torPreferences.bridgeCopy "کپی آدرس پل">
<!ENTITY torPreferences.copied "کپی‌ شد!">
<!ENTITY torPreferences.bridgeShowAll "نمایش همه پل‌ها">
<!ENTITY torPreferences.bridgeRemoveAll "حذف همه پل‌ها">
<!ENTITY torPreferences.bridgeAdd "افزودن یک پل جدید">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "یکی از پل‌های داخلی مرورگر تور را انتخاب کنید">
<!ENTITY torPreferences.bridgeSelectBuiltin "یک پل داخلی را انتخاب کنید…">
<!ENTITY torPreferences.bridgeRequest "درخواست یک پل...">
<!ENTITY torPreferences.bridgeEnterKnown "آدرس پلی که می‌دانید را وارد کنید">
<!ENTITY torPreferences.bridgeAddManually "افزودن دستی پل…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "پیشرفته ">
<!ENTITY torPreferences.advancedDescription "روش اتصال مرورگر تور به اینترنت را پیکربندی کنید">
<!ENTITY torPreferences.advancedButton "تنظیمات…">
<!ENTITY torPreferences.viewTorLogs "مشاهده وقایع تور">
<!ENTITY torPreferences.viewLogs "مشاهده وقایع...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "همه پل‌ها حذف شوند؟">
<!ENTITY torPreferences.removeBridgesWarning "این عمل قابل بازگشت نیست.">
<!ENTITY torPreferences.cancel "لغو">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "اسکن کد QR">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "پل‌های داخلی">
<!ENTITY torPreferences.builtinBridgeDescription "مرورگر تور انواع خاصی از پل‌ها با نام &quot;حامل‌های اتصال&quot; را شامل می‌شود.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 نوعی پل داخلی است که ترافیک تور شما را تصادفی جلوه می‌دهد. همچنین احتمال مسدود شدن آن نسبت به پل قبلی خود، یعنی obfs3، کمتر است.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake یک پل داخلی است که با مسیریابی اتصال شما از طریق پراکسی‌های Snowflake که توسط داوطلبان اداره می‌شود، سانسور را شکست می‌دهد.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure یک پل داخلی است که وانمود می‌کند شما به جای استفاده از تور از یک وب‌سایت مایکروسافت بازدید می‌کنید.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "پل درخواست کن">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "در حال تماس با BridgeDB. لطفا منتظر بمانید.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "برای درخواست یک پل کپچا را حل کنید.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "راه حل درست نیست. لطفا دوباره تلاش کنید.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "ارائه پل">
<!ENTITY torPreferences.provideBridgeHeader "وارد کردن اطلاعات پل از یک منبع مورد اعتماد">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "تنظیمات اتصال">
<!ENTITY torPreferences.connectionSettingsDialogHeader "روش اتصال مرورگر تور به اینترنت را پیکربندی کنید">
<!ENTITY torPreferences.firewallPortsPlaceholder "مقادیر جدا‌شده با ویرگول">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "وقایع Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "عدم اتصال">
<!ENTITY torConnect.connectingConcise "در حال اتصال...">
<!ENTITY torConnect.tryingAgain "در حال امتحان دوباره…">
<!ENTITY torConnect.noInternet "مرورگر Tor به اینترنت دسترسی پیدا نکرد">
<!ENTITY torConnect.noInternetDescription "این ممکن است به دلیل مشکل اتصال باشد، نه مسدود شدن Tor. قبل از تلاش دوباره، تنظیمات اتصال اینترنت، پروکسی و فایروال خود را بررسی کنید.">
<!ENTITY torConnect.couldNotConnect "مرورگر تور نمی‌تواند به تور متصل شود">
<!ENTITY torConnect.assistDescriptionConfigure "اتصال خود را پیکربندی کنید"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "اگر تور در موقعیت مکانی شما مسدود شده است، امتحان کردن یک پل می‌تواند است کمک‌کننده باشد. کمک اتصال می‌تواند با استفاده از موقعیت مکانی شما یک پل را برای شما انتخاب کند، و یا به جای آن می‌توانید به صورت دستی #1."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "در حال امتحان یک پل…">
<!ENTITY torConnect.tryingBridgeAgain "در حال تلاش برای یک بار دیگر…">
<!ENTITY torConnect.errorLocation "مرورگر تور قادر به مکان‌یابی شما نیست">
<!ENTITY torConnect.errorLocationDescription "مرورگر تور باید مکان شما را بداند تا پل مناسب شما را انتخاب کند. اگر ترجیح می‌دهید موقعیت مکانی خود را به اشتراک نگذارید، به جای آن به صورت دستی #1."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "آیا این تنظیمات مکانی درست هستند؟">
<!ENTITY torConnect.isLocationCorrectDescription "مرورگر Tor هنوز نمی‌تواند به Tor متصل شود. لطفا درستی تنظیمات موقعیت مکانی خود را بررسی کنید و دوباره امتحان کنید، یا به جای آن #1."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "مرورگر Tor هنوز نمی‌تواند متصل شود">
<!ENTITY torConnect.finalErrorDescription "علیرغم همه تلاش‌ها، دستیار اتصال قادر به اتصال به Tor نبود. اتصال خود را عیب‌یابی کنید و به جای آن یک پل را به صورت دستی اضافه کنید.">
<!ENTITY torConnect.breadcrumbAssist "کمک اتصال">
<!ENTITY torConnect.breadcrumbLocation "تنظیمات مکانی">
<!ENTITY torConnect.breadcrumbTryBridge "یک پل انتخاب کنید">
<!ENTITY torConnect.automatic "خودکار">
<!ENTITY torConnect.selectCountryRegion "کشور یا منطقه را انتخاب کنید">
<!ENTITY torConnect.frequentLocations "مکان‌های متداول">
<!ENTITY torConnect.otherLocations "سایر مکان‌ها">
<!ENTITY torConnect.restartTorBrowser "مرورگر تور را دوباره راه‌اندازی کنید">
<!ENTITY torConnect.configureConnection "پیکربندی اتصال...">
<!ENTITY torConnect.viewLog "مشاهده وقایع…">
<!ENTITY torConnect.tryAgain "دوباره سعی کنید">
<!ENTITY torConnect.offline "اینترنت در دسترس نیست">
<!ENTITY torConnect.connectMessage "تغییرات انجام شده در تنظیمات تور، تا زمان اتصال شما اعمال نخواهند شد">
<!ENTITY torConnect.tryAgainMessage "مرورگر Tor موفق به برقراری ارتباط با شبکه Tor نشده است">
<!ENTITY torConnect.yourLocation "موقعیت مکانی شما">
<!ENTITY torConnect.tryBridge "یک پل را امتحان کنید">
<!ENTITY torConnect.autoBootstrappingFailed "پیکربندی خودکار انجام نشد">
<!ENTITY torConnect.autoBootstrappingFailed "پیکربندی خودکار انجام نشد">
<!ENTITY torConnect.cannotDetermineCountry "تعیین کشور کاربر ممکن نیست">
<!ENTITY torConnect.noSettingsForCountry "هیچ تنظیماتی برای مکان شما موجود نیست">
